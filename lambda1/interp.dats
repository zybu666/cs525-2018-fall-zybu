(* ****** ****** *)

datatype value =
  | VALint of int
  | VALbool of bool
  | VALstring of string
  | VALclo of (term, envir)
  | VALfix of (term, envir)
  
where
envir = list0( $tup(tvar, value) )

(* ****** ****** *)

extern
fun
print_value : (value) -> void
and
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
print_value(t0) = 
fprint_value(stdout_ref, t0)
implement
prerr_value(t0) = 
fprint_value(stderr_ref, t0)

(* ****** ****** *)

local

implement
fprint_val<value> = fprint_value

in (* in-of-local *)

implement
fprint_value
  (out, v0) =
(
case+ v0 of
//
| VALint(x) =>
  fprint!(out, "VALint(", x, ")")
| VALbool(x) =>
  fprint!(out, "VALbool(", x, ")")
| VALstring(x) =>
  fprint!(out, "VALstring(", x, ")")
//
| VALclo _ => fprint!(out, "VALclo(...)")
| VALfix _ => fprint!(out, "VALfix(...)")
//
) (* end of [fprint_value] *)

end // end of [local]

(* ****** ****** *)

extern
fun
interp0(src: term): value
and
interp_env(src: term, env: envir): value

(* ****** ****** *)

implement
interp0(src) =
interp_env(src, list0_nil())

(* ****** ****** *)

local
//
static
fun
aux_var:
(tvar, envir) -> value
static
fun
aux_opr:
(term, envir) -> value
//
implement
aux_var(x0, xvs) = 
(
case- xvs of
| list0_cons(xv, xvs) =>
  if xv.0 = x0
    then xv.1 else aux_var(x0, xvs)
  // end of [if]
)
//
implement
aux_opr
(t0, env) = let
//
val-
TMopr(opr, ts) = t0
//
val vs =
list0_map<term><value>
  (ts, lam(t) => interp_env(t, env))
//
in
//
ifcase
| opr = "+" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1+i2)
  end
| opr = "-" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1-i2)
  end
| opr = "*" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1*i2)
  end
//
| opr = "<" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 < i2)
  end
| opr = "<=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 <= i2)
  end
| opr = ">" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 > i2)
  end
| opr = ">=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 >= i2)
  end
//
| _ (* else *) =>
    (prerrln!("lambda1: interp: aux_opr: opr = ", opr); exit(1))
//
end // end of [aux_opr]
//
in (* in-of-local *)

implement
interp_env(t0, env) =
(
case+ t0 of
//
| TMint(i) => VALint(i)
| TMbool(b) => VALbool(b)
| TMstring(s) => VALstring(s)
//
| TMvar(x) => aux_var(x, env)
//
| TMlam(x, t) => VALclo(t0, env)
//
| TMapp(t1, t2) => let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
  in
    case- v1 of
    | VALclo
      (t_fun, env_fun) => let
        val-TMlam(x, t_body) = t_fun
      in
        interp_env
        (t_body, cons0( $tup(x, v2), env_fun ))
      end
    | VALfix(t_fix, env_fun) => let
        val-TMfix(f, x, t_body) = t_fix
        val env_fun =
          list0_cons( $tup(f, v1), env_fun )
        val env_fun =
          list0_cons( $tup(x, v2), env_fun )
      in
        interp_env(t_body, env_fun)
      end
  end
//
| TMlet(x, t1, t2) => let
    val v1 = interp_env(t1, env)
  in
    interp_env
    (t2, cons0( $tup(x, v1), env ))
  end
//
| TMfix _ => VALfix(t0, env)
//
| TMift(t1, t2, t3) => let
    val v1 = interp_env(t1, env)
  in
    case- v1 of
    | VALbool(b) =>
      interp_env(ifval(b, t2, t3), env)
  end
//
| TMopr(opr, ts) => aux_opr(t0, env)
//
) (* end of [interp_env] *)

end // end of [local]

(* ****** ****** *)

(* end of [interp.dats] *)
