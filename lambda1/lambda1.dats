(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)

#include "./syntax.dats"
#include "./interp.dats"

(* ****** ****** *)
//
val
TMadd =
lam
( x: term
, y: term): term =>
  TMopr("+", x::y::nil0())
val
TMsub =
lam
( x: term
, y: term): term =>
  TMopr("-", x::y::nil0())
//
val
TMmul =
lam
( x: term
, y: term): term =>
  TMopr("*", x::y::nil0())
//
val
TMlt =
lam
( x: term
, y: term): term =>
  TMopr("<", x::y::nil0())
val
TMlte =
lam
( x: term
, y: term): term =>
  TMopr("<=", x::y::nil0())
//
val
TMgt =
lam
( x: term
, y: term): term =>
  TMopr(">", x::y::nil0())
val
TMgte =
lam
( x: term
, y: term): term =>
  TMopr(">=", x::y::nil0())
//
(* ****** ****** *)

val
TMfact = lam(arg: term): term => TMapp(TMfix( "f", "x", TMift( TMgte(x, TMint(1)), TMmul(x, TMapp(f, TMsub(x, TMint(1)))), TMint(1) (* else *))), arg) where
{
  val f = TMvar"f" and x = TMvar"x"
} (* end of [val] *)

(* ****** ****** *)

implement
main0(argc, argv) =
{
//
(*
val () =
println!("argc = ", argc)
val () =
println!("argv[0] = ", argv[0])
*)
//
val () =
println!("fact(5) = ", interp0(TMfact(TMint(5))))
val () =
println!("fact(10) = ", interp0(TMfact(TMint(10))))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [lambda1.dats] *)
