
#include
"share/atspre_staload.hats"


fun
fact(x: int): int =
if x > 0 then x*fact(x-1) else 1

(*
val fact = fix f(x: int): int => if x > 0 then x*f(x-1) else 1
*)
val rec fact: int -> int = fix f(x: int): int => if x > 0 then x*f(x-1) else 1

implement main0() =
{
val () = println! ("fact(10) = ", fact(10))
}
