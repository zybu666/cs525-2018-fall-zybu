(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)

datatype term =
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), term) // fixed-point opr
//
(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) =
fprint_term(stdout_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
//
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMfix(f1, x2, t3) =>
  fprint!(out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
//
)

end // end of [local]

(* ****** ****** *)

extern
fun
interp(src: term): term

(* ****** ****** *)

extern
fun
subst :
(term, tvar, term) -> term

#include "./subst.dats"

(* ****** ****** *)

local

fun
aux_app
(t0: term): term = let
//
val-TMapp(t1, t2) = t0
//
val t1 = interp(t1)
val t2 = interp(t2) // call-by-value
//
in
  case- t1 of
  | TMlam(x, t_body) =>
    interp(subst(t_body, x, t2))
end // end of [aux_app]

fun
aux_let
(t0: term): term = let
//
val-TMlet(x, t1, t2) = t0
//
in
  interp(subst(t2, x, interp(t1)))
end // end of [aux_opr]

fun
aux_opr
(t0: term): term = let
//
val-TMopr(opr, ts) = t0
//
val ts =
list0_map<term><term>(ts, lam(t) => interp(t))
//
in
//
ifcase
| opr = "+" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1+i2)
  end
| opr = "-" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1-i2)
  end
| opr = "*" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMint(i1*i2)
  end
//
| opr = "<" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 < i2)
  end
| opr = ">" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 > i2)
  end
| opr = "<=" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 <= i2)
  end
| opr = ">=" =>
  let
    val-
    TMint(i1)::TMint(i2)::nil0() = ts in TMbool(i1 >= i2)
  end
//
| _ (* else *) =>
    (prerrln!("interp: aux_opr: opr = ", opr); exit(1))
//
end // end of [aux_opr]

(*
static
fun aux_ift : term -> term
*)
fun
aux_ift
(t0: term): term = let
//
val-
TMift(t1, t2, t3) = t0
//
val t1 = interp(t1)
//
in
  case- t1 of
  | TMbool(tt) =>
    if tt then interp(t2) else interp(t3)
end // end of [aux_ift]

(*
static
fun aux_fix : term -> term
*)
fun
aux_fix
(t0: term): term = let
//
val-TMfix(f, x, t) = t0
//
in
  TMlam(x, subst(t, f, t0))
end // end of [aux_fix]

in (* in-of-local *)

implement
interp(t0) =
(
case+ t0 of
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
| TMvar _ => t0
| TMlam _ => t0
| TMapp _ => aux_app(t0)
| TMlet _ => aux_let(t0)
| TMopr _ => aux_opr(t0)
| TMift _ => aux_ift(t0)
| TMfix _ => aux_fix(t0)
)

end

(* ****** ****** *)

val K =
TMlam("x", TMlam("y", TMvar("x")))

(* ****** ****** *)

val S =
TMlam
("x"
, TMlam("y"
       , TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))
where
{
  val x = TMvar("x")
  val y = TMvar("y")
  val z = TMvar("z")
}

(* ****** ****** *)

val SKK = TMapp(TMapp(S, K), K)

(* ****** ****** *)

local

val x = TMvar"x"
val f = TMvar"f"

in (* in-of-local *)

val N2 = TMlam("f", TMlam("x", TMapp(f, TMapp(f, x))))
val N3 = TMlam("f", TMlam("x", TMapp(f, TMapp(f, TMapp(f, x)))))

val SUCC = TMlam("x", TMopr("+", TMvar"x" :: TMint(1) :: nil0))

end // end of [local]

(* ****** ****** *)

val N9 = TMapp(N2, N3)

val () =
println!
( "TMint(9) = "
, interp(TMapp(TMapp(N9, SUCC), TMint(0))))

(* ****** ****** *)

val
TMadd =
lam
( x: term
, y: term): term =>
  TMopr("+", x::y::nil0())
val
TMsub =
lam
( x: term
, y: term): term =>
  TMopr("-", x::y::nil0())
val
TMmul =
lam
( x: term
, y: term): term =>
  TMopr("*", x::y::nil0())
val
TMlte =
lam
( x: term
, y: term): term =>
  TMopr("<=", x::y::nil0())
val
TMgte =
lam
( x: term
, y: term): term =>
  TMopr(">=", x::y::nil0())

(* ****** ****** *)

val
TMfact = TMfix( "f", "x", TMift( TMgte(x, TMint(1)), TMmul(x, TMapp(f, TMsub(x, TMint(1)))), TMint(1)))
where
{
  val f = TMvar"f" and x = TMvar"x"
} (* end of [val] *)

(* ****** ****** *)

val
TMfib = TMfix( "f", "x", TMift( TMgte(x, TMint(2)), TMadd(TMapp(f, TMsub(x, TMint(1))), TMapp(f, TMsub(x, TMint(2)))), x)
) where
{
  val f = TMvar"f" and x = TMvar"x"
} (* end of [val] *)

(* ****** ****** *)

implement
main0() =
{
//
  val () = println!("K = ", K)
  val () = println!("S = ", S)
//
(*
  val () = println!("SKK = ", SKK)
*)
//
  val x = TMvar"x"
  val () = println!("subst = ", subst(K,"z",S))
  val x0 = TMvar"x0"
  val () = println!("SKK(x0) = ", interp(TMapp(SKK, x0)))
//
  val () = println!("fact(10) = ", interp(TMapp(TMfact, TMint(10))))
//
  val () = println!("fib(10) = ", interp(TMapp(TMfib, TMint(10))))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [lecture-09-20.dats] *)
