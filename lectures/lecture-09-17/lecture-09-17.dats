(* ****** ****** *)

typedef tvar = string

(* ****** ****** *)

datatype term =
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
  
(* ****** ****** *)

extern
fun
print_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
)

(* ****** ****** *)

extern
fun
interp(src: term): term

(* ****** ****** *)

extern
fun
subst :
(term, tvar, term) -> term

(* ****** ****** *)

implement
subst(t0, x, t) =
(
case+ t0 of
//
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
//
| TMvar y =>
  if x = y then t else t0
//
| TMlam(y, t1) =>
  if x = y
  then t0 else TMlam(y, subst(t1, x, t))
  // end of [if]
//
| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))
//
)

(* ****** ****** *)

local

fun
aux_app
(t0: term): term = let
//
val-TMapp(t1, t2) = t0
//
val t1 = interp(t1)
val t2 = interp(t2) // call-by-value
//
in
  case- t1 of
  | TMlam(x, t) => interp(subst(t, x, t2))
end // end of [aux_app]

in (* in-of-local *)

implement
interp(t0) =
(
case+ t0 of
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
| TMvar _ => t0
| TMlam _ => t0
| TMapp _ => aux_app(t0)
)

end

(* ****** ****** *)

val K =
TMlam("x", TMlam("y", TMvar("x")))

(* ****** ****** *)

val S =
TMlam
("x"
, TMlam("y"
       , TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))
where
{
  val x = TMvar("x")
  val y = TMvar("y")
  val z = TMvar("z")
}

(* ****** ****** *)

val SKK = TMapp(TMapp(S, K), K)

(* ****** ****** *)

implement
main0() =
{
//
  val () = println!("K = ", K)
  val () = println!("S = ", S)
//
(*
  val () = println!("SKK = ", SKK)
*)
//
  val x0 = TMvar"x0"
  val () = println!("SKK(x0) = ", interp(TMapp(SKK, x0)))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [lecture-09-17.dats] *)
