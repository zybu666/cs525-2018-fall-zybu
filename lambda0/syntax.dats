(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
//
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), term) // fixed-point opr
//  
(* ****** ****** *)
//
typedef termlst = list0(term)
//
(* ****** ****** *)

extern
fun
print_term : (term) -> void
and
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)
implement
prerr_term(t0) = 
fprint_term(stderr_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
//
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
//
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
//
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
//
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
//
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
//
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMfix(f1, x2, t3) =>
  fprint!(out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
//
)

end // end of [local]

(* ****** ****** *)

(* end of [syntax.dats] *)
