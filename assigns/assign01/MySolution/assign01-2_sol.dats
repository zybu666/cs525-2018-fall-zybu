(* ****** ****** *)
//
// How to test
// ./assign01-2_sol_dats
//
// How to compile:
// myatscc assign01-2_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01-2.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
(* ****** ****** *)
extern
fun
print_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload fprint with fprint_term

(* ****** ****** *)
implement
print_term(t0) =
fprint_term(stdout_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
)

end

extern
fun
subst_helper(tlist:termlst, x: tvar, t0: term): termlst
implement
subst_helper(tlist:termlst, x: tvar, t0: term): termlst =
case+ tlist of
| nil0() => nil0()
| cons0(t, ts) => cons0(subst(t, x, t0), subst_helper(ts,x,t0))

implement
subst(t0, x, t) =
(
case+ t0 of
//
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
| TMvar(a) => if x = a then t else t0
| TMlam(a, t1) => if x = a then t0 else TMlam(a, subst(t1,x,t))
| TMapp(t1,t2) => TMapp(subst(t1, x, t), subst(t2, x, t))
| TMlet(a, t1, t2) => TMlet(a, t1, t2) where{
    val t1 = subst(t1,x,t)
    val t2 = if x = a then t2 else subst(t2,x,t)
}
| TMopr(opr, ts) => TMopr(opr, subst_helper(ts, x, t))
//
)

implement
main0() = {

val x = TMvar"x"
val y = TMvar"y"
val z = TMvar"z"

val S= TMapp(TMlam("z", z), TMlam("y", TMapp(y, z)))
val K = subst(S, "z", x)
val() = println!("S = ", S)
val() = println!("K= ", K)
}
(* ****** ****** *)

(* end of [assign01-2_sol.dats] *)
