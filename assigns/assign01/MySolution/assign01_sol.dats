(* ****** ****** *)
//
// How to test
// ./assign01_sol_dats
//
// How to compile:
// myatscc assign01_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
implement
tvar_count(t0: term, x: string): int = 
case+ t0 of
| TMint(i) => 0
| TMvar(a) => 
if x = a then 1
else 0
| TMlam(a,t1) => if x = a then 0 
else tvar_count(t1,x)
| TMapp(t1,t2) => tvar_count(t1,x) + tvar_count(t2,x)
//
extern
fun
var_in_list(x: tvar, vlist: list0(tvar)): bool
implement
var_in_list(x: tvar, vlist: list0(tvar)): bool =
case+ vlist of
| nil0() => false
| cons0(v, vs) => if x =  v then true 
else var_in_list(x, vs)
//
extern 
fun
term_is_closed_helper(t0: term, vlist: list0(tvar)): bool
implement
term_is_closed_helper(t0: term, vlist: list0(tvar)): bool = 
case+ t0 of
| TMint(i) => true
| TMvar(a) => if var_in_list(a, vlist) then true else false
| TMlam(a, t1) => term_is_closed_helper(t1, cons0(a, vlist))
| TMapp(t1, t2) => term_is_closed_helper(t1,vlist) && term_is_closed_helper(t2, vlist)

implement
term_is_closed(t0:term): bool = 
term_is_closed_helper(t0, nil0())

implement
main0() = {
val J = TMlam("y", TMvar("x"))
val () = println!("J = ", J)
val () = println!(tvar_count(J, "x"))
val K = TMlam("x", TMlam("y", TMvar("x")))
val () = println!("K = ", K)
val () = println!(tvar_count(K,"x"))
val S =
TMlam
("x"
, TMlam("y"
       , TMapp(TMapp(x, z), TMapp(y, z))))
where
{
  val x = TMvar("x")
  val y = TMvar("y")
  val z = TMvar("z")
}
val () = println!("S = ", S)
val () = println!(tvar_count(S, "z"))

val x = TMvar"x"
val y = TMvar"y"
val z = TMvar"z"

val omega = TMlam("x", x)
val Omega = TMapp(omega, omega)

val K = TMlam("x", TMlam("y", x))
val K' = TMlam("x", TMlam("y", y))

val () = assertloc(term_is_closed(K))
val () = assertloc(term_is_closed(K'))
val () = assertloc(term_is_closed(omega))
val () = assertloc(term_is_closed(Omega))

val () = assertloc(~term_is_closed(x))
val () = assertloc(~term_is_closed(TMapp(x, y)))

val() = println!("pass the close test!!!")
}

(* ****** ****** *)

(* end of [assign01_sol.dats] *)
