

local

val x = TMvar"x"
val y = TMvar"y"
val z = TMvar"z"

val omega = TMlam("x", x)
val Omega = TMapp(omega, omega)

val K = TMlam("x", TMlam("y", x))
val K' = TMlam("x", TMlam("y", y))

val S = TMlam("x", TMlam("y", TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))

val SKK = TMapp(TMapp(S, K), K)

in

  println! ("SKK(100) = ", TMapp(SKK, TMint(100)))

end (* end of [local] *)