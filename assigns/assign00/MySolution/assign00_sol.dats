(* ****** ****** *)
//
// How to test
// ./assign00_sol_dats
//
// How to compile:
// myatscc assign00_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign00.dats"

(* ****** ****** *)

implement
factorial(n) =
if n > 0
then n * factorial(n-1) else 1


implement
int_test():int = let
fun loop(n:int,c:int):int =
if n > 0 then loop(n+n,c+1)
else c
in
loop(1,1)
end


implement
gheep(n:int): int = let
fun
loop(c:int,a:int,b:int):int =
if n = 0 then 1
else
if c < n+1 then loop(c+1,b,c * a * b)
else b
in
loop(2,1,2)
end

fun
appendloop(xs:intlist,ys:intlist): intlist =
case+ xs of
| nil() => ys
| cons(x,xs) => appendloop(xs,cons(x,ys))


implement
intlist_append(xs,ys)=
appendloop(appendloop(xs,nil()),ys)

(* ****** ****** *)

implement
main0() =
{

val () =
println!
("factorial(10) = ", factorial(10))

val () =
println!
("int_test() = ", int_test())

val () = 
assertloc(ghaap(0) = gheep(0))
val () = 
assertloc(ghaap(1) = gheep(1))
val() = 
assertloc(ghaap(2) = gheep(2))
val() = 
assertloc(ghaap(3) = gheep(3))
val() = 
assertloc(ghaap(4) = gheep(4))
val() = 
assertloc(ghaap(5) = gheep(5))
val() = 
assertloc(ghaap(6) = gheep(6))
val() =
println!
("ghaap test pass!!!")

val xs = cons(0, cons(1, cons(2, nil())))
val ys = cons(3, cons(4, cons(5, nil())))
val () = fprintln! (stdout_ref, "appendlist = ", $UNSAFE.cast{list0(int)}(intlist_append(xs, ys)))

} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign00_sol.dats] *)
