(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 525
//
// Semester: Fall, 2018
//
// Classroom: FLR 112
// Class Time: TR 2:00-3:15
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 9th of October
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
//
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), term) // fixed-point opr
//
  | TMtupl of list0(term)
  | TMproj of (term, int(*index*)) // index starts from 0
//
(* ****** ****** *)

datatype value =
//
  | VALint of int
  | VALbool of bool
  | VALstring of string
//
  | VALclo of (term, envir)
  | VALfix of (term, envir)
//
  | VALtupl of list0(value)
//
where
envir = list0( $tup(tvar, value) )

(* ****** ****** *)
//
// HX: 10 points
// Please implement a term
// that test whether a given
// natural number is a prime
//
fun
isprime(x: int) =
(
if x >= 2 then test(2) else false
) where
{
fun
test(i: int): bool =
if
i < x
then
  (if x % i = 0 then false else test(i+1))
else true
}
//
extern
val TMisprime : term
//
// The term essentially encodes 'isprime'
//
(* ****** ****** *)
//
// HX: 20: point
// Please read the implementation of lambda1.
// Then please extend it with support for tuples.
//
extern
fun interp0 : term -> value
//
(* ****** ****** *)
//
// HX: 30: point
//
// This one is a bit of challenging :)
//
// An implementation of the 8-queen puzzle is given below:
// http://ats-lang.sourceforge.net/DOCUMENT/INT2PROGINATS/HTML/HTMLTOC/x631.html
//
// Please implement a function of the following type such that
// interp0(TMqueenpuzzle(N)) prints out all the solutions to the N-queen puzzle
// In case N = 8, there are 92 solutions.
//
extern
fun TMqueenpuzzle(N: int) : term
//
(* ****** ****** *)

(* end of [assign02.dats] *)
