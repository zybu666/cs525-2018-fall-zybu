(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 525
//
// Semester: Fall, 2018
//
// Classroom: FLR 112
// Class Time: TR 2:00-3:15
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 9th of October
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
#define :: list0_cons

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
//
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of (tvar(*f*), tvar(*x*), term) // fixed-point opr
//
  | TMtupl of list0(term)
  | TMproj of (term, int(*index*)) // index starts from 0
//
(* ****** ****** *)

datatype value =
//
  | VALint of int
  | VALbool of bool
  | VALstring of string
//
  | VALclo of (term, envir)
  | VALfix of (term, envir)
//
  | VALtupl of list0(value)
//
where
envir = list0( $tup(tvar, value) )

(* ****** ****** *)

extern
fun
print_value : (value) -> void
and
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)
(* ****** ****** *)

implement
print_value(t0) =
fprint_value(stdout_ref, t0)
implement
prerr_value(t0) =
fprint_value(stderr_ref, t0)

(* ****** ****** *)
(* ****** ****** *)

local

implement
fprint_val<value> = fprint_value

in (* in-of-local *)

implement
fprint_value
  (out, v0) =
(
case+ v0 of
//
| VALint(x) =>
  fprint!(out, "VALint(", x, ")")
| VALbool(x) =>
  fprint!(out, "VALbool(", x, ")")
| VALstring(x) =>
  fprint!(out, "VALstring(", x, ")")
//
| VALclo _ => fprint!(out, "VALclo(...)")
| VALfix _ => fprint!(out, "VALfix(...)")
| VALtupl(xs) => fprint!(out,"VALtupl(", xs, ")")
//
) (* end of [fprint_value] *)

end // end of [local]

(* ****** ****** *)
(* ****** ****** *)

extern
fun
print_term : (term) -> void
and
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)
implement
print_term(t0) =
fprint_term(stdout_ref, t0)
implement
prerr_term(t0) =
fprint_term(stderr_ref, t0)

local

implement
fprint_val<term> = fprint_term

in

implement
fprint_term
  (out, t0) =
(
case+ t0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, t) =>
  fprint!(out, "TMlam(", x, "; ", t, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts, ")")
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMfix(f1, x2, t3) =>
  fprint!(out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
| TMtupl(xs) =>
  (fprint!(out, "TMtupl()"); list0_foreach(xs, lam(x) => println!(x)))
| TMproj(t1, i) =>
  fprint!(out,"TMproj(", t1, "|", i, ")")
//
)

end

val
TMadd =
lam
( x: term
, y: term): term =>
  TMopr("+", x::y::nil0())
val
TMsub =
lam
( x: term
, y: term): term =>
  TMopr("-", x::y::nil0())
val
TMmul =
lam
( x: term
, y: term): term =>
  TMopr("*", x::y::nil0())
val
TMlte =
lam
( x: term
, y: term): term =>
  TMopr("<=", x::y::nil0())
val
TMgte =
lam
( x: term
, y: term): term =>
  TMopr(">=", x::y::nil0())
val
TMmod =
lam
(x:term, y: term): term =>
TMopr("%", x::y::nil0())
val
TMlt =
lam(x: term, y: term): term => TMopr("<", x::y::nil0())

//
// HX: 10 points
// Please implement a term
// that test whether a given
// natural number is a prime
//
fun
isprime(x: int) =
(
if x >= 2 then test(2) else false
) where
{
fun
test(i: int): bool =
if
i < x
then
  (if x % i = 0 then false else test(i+1))
else true
}
//
extern
val TMisprime : term

implement
TMisprime =
TMfix("f", "x", TMift(TMgte(x,TMint(2)), TMapp(TMtest,TMint(2)), TMbool(false)))
where
{
  val f = TMvar"f" and x = TMvar"x" and i = TMvar"i"
  val TMtest: term =
  TMfix("f", "i", TMift(TMlt(i, x),TMift(TMmod(x,i), TMbool(false), TMapp(f,TMadd(i,TMint(1)))),TMbool(true)))
}
//
// The term essentially encodes 'isprime'
//
(* ****** ****** *)
//
// HX: 20: point
// Please read the implementation of lambda1.
// Then please extend it with support for tuples.
//
extern
fun interp0 : term -> value
and
interp_env: (term, envir) -> value

implement
interp0(src) =
interp_env(src, list0_nil())
//
(* ****** ****** *)

local
//
static
fun
aux_var:
(tvar, envir) -> value
static
fun
aux_opr:
(term, envir) -> value
//
implement
aux_var(x0, xvs) =
(
case- xvs of
| list0_cons(xv, xvs) =>
  if xv.0 = x0
    then xv.1 else aux_var(x0, xvs)
  // end of [if]
)
//
implement
aux_opr
(t0, env) = let
//
val-
TMopr(opr, ts) = t0
//
val vs =
list0_map<term><value>
  (ts, lam(t) => interp_env(t, env))
//
in
//
ifcase
| opr = "+" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1+i2)
  end
| opr = "-" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1-i2)
  end
| opr = "*" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1*i2)
  end
//
| opr = "<" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 < i2)
  end
| opr = "<=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 <= i2)
  end
| opr = ">" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 > i2)
  end
| opr = ">=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 >= i2)
  end
| opr = "%" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 % i2 = 0)
  end
//
| _ (* else *) =>
    (prerrln!("lambda1: interp: aux_opr: opr = ", opr); exit(1))
//
end // end of [aux_opr]
//
in (* in-of-local *)

implement
interp_env(t0, env) =
(
case- t0 of
//
| TMint(i) => VALint(i)
| TMbool(b) => VALbool(b)
| TMstring(s) => VALstring(s)
//
| TMvar(x) => aux_var(x, env)
//
| TMlam(x, t) => VALclo(t0, env)
//
| TMapp(t1, t2) => let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
  in
    case- v1 of
    | VALclo
      (t_fun, env_fun) => let
        val-TMlam(x, t_body) = t_fun
      in
        interp_env
        (t_body, cons0( $tup(x, v2), env_fun ))
      end
    | VALfix(t_fix, env_fun) => let
        val-TMfix(f, x, t_body) = t_fix
        val env_fun =
          list0_cons( $tup(f, v1), env_fun )
        val env_fun =
          list0_cons( $tup(x, v2), env_fun )
      in
        interp_env(t_body, env_fun)
      end
  end
//
| TMlet(x, t1, t2) => let
    val v1 = interp_env(t1, env)
  in
    interp_env
    (t2, cons0( $tup(x, v1), env ))
  end
//
| TMfix _ => VALfix(t0, env)
//
| TMift(t1, t2, t3) => let
    val v1 = interp_env(t1, env)
  in
    case- v1 of
    | VALbool(b) =>
      interp_env(ifval(b, t2, t3), env)
  end
//
| TMopr(opr, ts) => aux_opr(t0, env)
| TMtupl(ts) => VALtupl(list0_map<term><value>(ts, lam(t) => interp_env(t, env)))
| TMproj(t, i) => let
    val v1 = interp_env(t, env)
    in
    case- v1 of
    | VALtupl(xs) => xs[i]
    end
//
)

end // end of [local]

(* ****** ****** *)
(* ****** ****** *)
//
// HX: 30: point
//
// This one is a bit of challenging :)
//
// An implementation of the 8-queen puzzle is given below:
// http://ats-lang.sourceforge.net/DOCUMENT/INT2PROGINATS/HTML/HTMLTOC/x631.html
//
// Please implement a function of the following type such that
// interp0(TMqueenpuzzle(N)) prints out all the solutions to the N-queen puzzle
// In case N = 8, there are 92 solutions.
//

//
extern
fun TMqueenpuzzle(N: int) : term
implement TMqueenpuzzle(N: int): term =
let
fun
show(bd:term): term =

//
fun
safe
(i: int, j: int, k: int, xs: list0(int)) : bool =
(
  case+ xs of
  | nil0() => true
  | cons0(x, xs) => x != i && x != j && x != k && safe(i, j+1, k-1, xs)
)
//
fun
loop
(col: int, xs: list0(int)) : void =
(N).foreach()
(
lam(i) =>
if
safe(i, i+1, i-1, xs)
then let
  val xs = cons0(i, xs)
in
  if col = N then show(xs) else loop(col+1, xs)
end // end of [then]
)
//
in
  loop(1, nil0())
end

//
(* ****** ****** *)
implement
main0() =
{
  val () = println!("isprime(100) = ", interp0(TMapp(TMisprime, TMint(98))))
  val () = println!("isprime(23) = ", interp0(TMapp(TMisprime, TMint(23))))
  val () = println!("isprime(2) = ", interp0(TMapp(TMisprime, TMint(2))))
  val () = println!("isprime(91) = ", interp0(TMapp(TMisprime, TMint(91))))

}
(* end of [assign02.dats] *)
